import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;


public class XMLtoJSON {

	public static int PRETTY_PRINT_INDENT_FACTOR = 4;
	
	private String readFile( String file ) throws IOException {
		BufferedReader reader = new BufferedReader( new FileReader (file));
		String line = null;
		StringBuilder stringBuilder = new StringBuilder();
		String ls = System.getProperty("line.separator");

		while( ( line = reader.readLine() ) != null ) {
			stringBuilder.append( line );
			stringBuilder.append( ls );
		}

		return stringBuilder.toString();
		}

	public static void main(String[] args) {
		XMLtoJSON obj=new XMLtoJSON();
		try {
			File file=new File(args[0]);
			String str=obj.readFile(file.getAbsolutePath());
			JSONObject xmlJSONObj = XML.toJSONObject(str);
			String jsonPrettyPrintString = xmlJSONObj.toString(PRETTY_PRINT_INDENT_FACTOR);
			System.out.println(jsonPrettyPrintString);
		} catch (JSONException je) {
			System.out.println(je.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}