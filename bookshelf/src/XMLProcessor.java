import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class XMLProcessor {
	public int validate(File file){
		int returncode=0;
		try {     
			SchemaFactory factory=SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);             // Create SchemaFactory instance for understanding W3C XML schemas
			Source schemaFile=new StreamSource(new File("src/bookdetails.xsd"));  							 // Load a W3C schema
			Schema schema=factory.newSchema(schemaFile);
			Validator validator=schema.newValidator();                                                       // Create Validator instance
			validator.validate(new StreamSource(file.getAbsolutePath()));									 // Validate the file (from given path) against XSD
			returncode=1;
		}
		catch (SAXException e) {
			returncode=-1;
		} catch (IOException e) {
			returncode=-1;
			e.printStackTrace();
		}
		return returncode;
	}
	public void displayList(File file){
		ArrayList<String> list=new ArrayList<String>();
		try {
			DocumentBuilder db=DocumentBuilderFactory.newInstance().newDocumentBuilder();						// Create new DocumentBuilderFactory instance to produce the parser for constructing DOM tree from XML
			Document doc=db.parse(file);																		// The parsed XML is stored in a Document object
			NodeList bookList=doc.getElementsByTagName("c:bookName");											// Create NodeList to access all bookNames				
			NodeList authorList=doc.getElementsByTagName("a:author");											// Create NodeList to access all authors
			for(int s=0;s<bookList.getLength();s++){
				list.add(authorList.item(s).getTextContent() + ":" + " " + bookList.item(s).getTextContent());;
			}
			for(String row:list){
				System.out.print(row);
			}
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {		
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static void main(String[] args){
		XMLProcessor obj=new XMLProcessor();
		File file=new File(args[0]);
		if(args[1].equalsIgnoreCase("validate")){
			int n=obj.validate(file);
			if(n==1){
				System.out.println("File is valid");
			}else{
				System.out.println("File is invalid");
			}
		}
		if(args[1].equalsIgnoreCase("list")){
			int n=obj.validate(file);
			if(n==1){
				System.out.println("File is valid");
			}else{
				System.out.println("File is invalid");
			}
			if(n==1){
				obj.displayList(file);
			}
		}
	}
}
