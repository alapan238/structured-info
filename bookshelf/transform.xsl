<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:c="group2_bookshelf" 
	xmlns:a="group2_author"
	xmlns:d="group2_publisher"
>
	<xsl:template match="/">
		<html>
			<body>
				<div align="center">
				<table border="1">
					<xsl:for-each select="c:database">
						<xsl:for-each select="c:bookDetails">
							<tr>
								<th>Book Name</th>
								<th>Author</th>
								<th>Publisher</th>
								<th>Edition</th>
								<th>Year of publishing</th>
							</tr>
							<tr>
								<td><xsl:value-of select="c:bookName"></xsl:value-of></td>
								<td><xsl:value-of select="a:author"></xsl:value-of></td>
								<td><xsl:value-of select="d:pubdet"></xsl:value-of></td>
							</tr>
					</xsl:for-each>
				</xsl:for-each>
				</table>
				</div>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>