<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:c="group2_bookshelf" 
	xmlns:a="group2_author"
	xmlns:d="group2_publisher"
>
	<xsl:template match="c:database">
		<html>
			<body>
				<div align="center">
				<table border="1">	
					<tr>
						<th>Book Name</th>
						<th>Author</th>
						<th>Publisher</th>
						<th>Edition</th>
						<th>Year of publishing</th>
					</tr>
					<xsl:for-each select="c:bookDetails">
						<tr>
							<td><xsl:value-of select="c:bookName"></xsl:value-of></td>
							<td><xsl:value-of select="a:author"></xsl:value-of></td>
							<xsl:apply-templates select="d:pubdet"/>
						</tr>
					</xsl:for-each>
				</table>
				</div>
			</body>
		</html>
	</xsl:template>


	<xsl:template match="d:pubdet">
		<td><xsl:value-of select="d:publisher"></xsl:value-of></td>
		<td><xsl:value-of select="d:edition"></xsl:value-of></td>
		<td><xsl:value-of select="d:year_of_publishing"></xsl:value-of></td>
	</xsl:template>
</xsl:stylesheet>
